import dialogflow from '@google-cloud/dialogflow';
import * as uuid from 'uuid';

export class RequestController {
    projectId = 'digitalbartender-fiqo';
    localSessionPath = null;
    sessionClient = null;

    initConnection = async function() {
        // A unique identifier for the given session
        let sessionId = uuid.v4();

        // Create a new session
        this.sessionClient = new dialogflow.SessionsClient();
        this.localSessionPath = this.sessionClient.projectAgentSessionPath(
            this.projectId,
            sessionId
        );
    }

    runQuery = async function (queryText, reqObject) {
        const request = {
            session: reqObject.localSessionPath,
            queryInput: {
                text: {
                    // The query to send to the dialogflow agent
                    text: `${queryText}`,
                    // The language used by the client (en-US)
                    languageCode: 'en-US',
                },
            },
        };


        try {
            return await reqObject.sessionClient.detectIntent(request);
        } catch (e) {
            console.log(e);
        }
    }
}

export const requestController = new RequestController();


